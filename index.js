// Require function will allow you to call the module
const readline = require('readline');
const calculateRectangleArea = require('./rectangle.js');
const calculateTriangleArea = require('./triangle.js');
const calculateCircleArea = require('./circle.js');
const calculateFiboNumber = require('./fibonacci.js');
const calculateFactorialNumber = require('./factorial.js');

// Initiate the readline to use stdin and stdout as input and output
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Start up message
console.clear(); // To clear up the terminal
console.log("Which one do you want to calculate?");
console.log(`
1. Rectangle Area
2. Triangle Area
3. Circle Area
4. Fibonacci Number
5. Factorial Number
`)

function handleAnswer1() {
  console.clear();
  console.log("Calculate Rectangle Area");

  // Ask for the width
  rl.question("Width: ", width => {

    // Ask for the length
    rl.question("Length: ", length => {
      console.log(
        "Here's the result:",
        calculateRectangleArea(width, length)
      );

      rl.close();
    })
  })
}

function handleAnswer2() {
  console.clear();
  console.log("Calculate Triangle Area");

  // Ask for the width
  rl.question("Base: ", base => {

    // Ask for the length
    rl.question("Height: ", height => {
      console.log(
        "Here's the result:",
        calculateTriangleArea(base, height)

      );

      rl.close();
    })
  })
}

function handleAnswer3() {
  console.clear();
  console.log("Calculate Circle Area");
  // Ask for the width
  rl.question("radian: ", radian => {
    console.log(
      "Here's the result:",
      calculateCircleArea(radian)

    );
    rl.close();
  })
}

function handleAnswer4() {
  console.clear();
  console.log("Calculate Fibonacci Number");
  // Ask for the width
  rl.question("number: ", number => {
    console.log(
      "Here's the result:",
      calculateFiboNumber(number)
    );
    rl.close();
  })
}

function handleAnswer5() {
  console.clear();
  console.log("Calculate Factorial Number");
  // Ask for the width
  rl.question("number: ", number => {
    console.log(
      "Here's the result:",
      calculateFactorialNumber(number)
    );
    rl.close();
  })
}
// Ask the width
rl.question('Answer: ', answer => {
  if (answer == 1) {
    handleAnswer1();
  } 
  else if (answer == 2) {
    handleAnswer2();
  } 
  else if (answer == 3) {
    handleAnswer3();
  } 
  else if (answer == 4){
    handleAnswer4();
  }
  else if (answer == 5){
    handleAnswer5();
  }
  else {
    console.log("Sorry, there's no option for that!")
    rl.close()
  }
});
